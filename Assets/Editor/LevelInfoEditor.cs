﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using Match3;


[CustomEditor(typeof(LevelInfo))]
public class MyScriptEditor : Editor
{
    public override void OnInspectorGUI()
    {
        var levelInfo = target as LevelInfo;

        var boardSize = serializedObject.FindProperty("BoardSize");
        var missions = serializedObject.FindProperty("Missions");        
        serializedObject.Update();
        EditorGUILayout.PropertyField(boardSize, true);
        EditorGUILayout.PropertyField(missions, true);        

        levelInfo.Limitation = (LevelLimitation)EditorGUILayout.EnumPopup("Limitation", levelInfo.Limitation);

        if (levelInfo.Limitation == LevelLimitation.Time)
            levelInfo.Time = EditorGUILayout.IntField("Time (in sec.):", levelInfo.Time);
        if (levelInfo.Limitation == LevelLimitation.Turns)
            levelInfo.Turns = EditorGUILayout.IntField("Turns limit:", levelInfo.Turns);
        levelInfo.CellTypeCount = EditorGUILayout.IntSlider("Cell Type Count", levelInfo.CellTypeCount, 5, 7);
        serializedObject.ApplyModifiedProperties();

        EditorUtility.SetDirty(levelInfo);
    }
}

