﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Match3.Core;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Match3
{
    class LevelManager : MonoBehaviour
    {
        private LevelInfo info;
        private MissionElement[] activeMissions;
        private int turnsLeft;
        private float timeLeft;
        private Dictionary<CellElement, int> cellCounter;

        public Board Board;
        public Text TurnsLeftCounter;
        public Text TimeLeftCounter;
        // Description of level limitation
        public Text LimitationLable;
        public Transform MissionsContainer;
        public MissionElement MissionElementPrefab;
        public LevelUI UI;


        private void Awake()
        {
//#if UNITY_EDITOR
            //if (DataManager.Instance == null) {
            //    info = Resources.LoadAll<LevelInfo>("Levels").First();
            //} else {
            //    info = DataManager.Instance.CurrentLevel;
            //}
//#else
            info = DataManager.Instance.CurrentLevel;
//#endif


            activeMissions = new MissionElement[info.Missions.Length];
            for (int i = 0; i < info.Missions.Length; i++) {
                var missionInfo = info.Missions[i];
                var mission = Instantiate(MissionElementPrefab, MissionsContainer);
                mission.ElementLeft = missionInfo.Count;
                mission.Element = missionInfo.Element;
                mission.ElementTile.sprite = Board.Tiles[(int)missionInfo.Element];
                activeMissions[i] = mission;
            }

            switch (info.Limitation) {
                case LevelLimitation.Time:
                    LimitationLable.text = "time left";
                    timeLeft = info.Time;
                    break;
                case LevelLimitation.Turns:
                    LimitationLable.text = "turns left";
                    turnsLeft = info.Turns;
                    TurnsLeftCounter.text = turnsLeft.ToString();
                    break;
            }
            Board.Size = info.BoardSize;
            Board.TileCount = Mathf.Max(5, info.CellTypeCount);
            cellCounter = new Dictionary<CellElement, int>(Board.TileCount);

            Board.OnTurnEnd += TurnEnd;
            Board.OnMatch += MatchComplete;
        }

        private void MatchComplete(MatchResult matchResult)
        {
            cellCounter.Clear();
            foreach (var match in matchResult.Matches) {
                foreach (var cell in match.Cells) {
                    if (!cellCounter.ContainsKey(cell.Element))
                        cellCounter[cell.Element] = 0;

                    cellCounter[cell.Element] += 1;
                }
            }

            bool levelComplete = true;

            foreach (var mission in activeMissions) {
                int elementCount;
                if (cellCounter.TryGetValue(mission.Element, out elementCount)) {
                    mission.ElementLeft = mission.ElementLeft - elementCount;
                }
                if (mission.ElementLeft > 0) {
                    levelComplete = false;
                }
            }

            if (levelComplete) {
                CompleteLevel();
            }
        }

        private void CompleteLevel()
        {
            Debug.Log("Complete level");
            Board.IsPaused = true;
            UI.ShowWinPanel();
        }

        private void TurnEnd()
        {
            if (info.Limitation == LevelLimitation.Turns) {
                turnsLeft--;
                TurnsLeftCounter.text = turnsLeft.ToString();

                if (turnsLeft <= 0) {
                    GameOver();
                }
            }
        }

        private void GameOver()
        {
            Debug.Log("Game over");
            Board.IsPaused = true;
            UI.ShowGameOverPanel();
        }

        public void PauseGame()
        {
            Board.IsPaused = true;
            UI.ShowPausePanel();
        }

        public void ResumeGame()
        {
            Board.IsPaused = false;
            UI.HidePausePanel();
        }

        public void RestartLevel()
        {
            SceneManager.LoadScene("game");
        }

        private void Update()
        {
            if (info.Limitation == LevelLimitation.Time && !Board.IsPaused) {
                timeLeft -= Time.deltaTime;
                int secondLeft = Mathf.RoundToInt(timeLeft);
                TimeLeftCounter.text = $"{secondLeft / 60}:{(secondLeft % 60):00}";
                if (timeLeft < 0) {
                    GameOver();
                }
            }
        }

        public void BackToMainMenu()
        {
            SceneManager.LoadScene("main menu");
        }

        public void LoadNextLevel()
        {
            int levelIdx = Array.IndexOf(DataManager.Instance.Levels, DataManager.Instance.CurrentLevel);
            if (levelIdx < DataManager.Instance.Levels.Length - 1) {
                DataManager.Instance.CurrentLevel = DataManager.Instance.Levels[levelIdx + 1];
                SceneManager.LoadScene("game");
            }
            else {
                SceneManager.LoadScene("main menu");
            }
        }
    }
}
