﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Match3.Core;
using UnityEngine;

namespace Match3
{
    [System.Serializable]
    public class LevelMission
    {
        public CellElement Element;
        public int Count;
    }

    public enum LevelLimitation { Time, Turns }

    [CreateAssetMenu(menuName = "Match3/Level info")]
    public class LevelInfo : ScriptableObject
    {
        public Vector2Int BoardSize;
        public LevelMission[] Missions;
        [Header("Limitation")]
        public LevelLimitation Limitation;
        public int Turns;
        [Tooltip("Time for level in sec.")]
        public int Time;
        public int CellTypeCount;
    }
}
