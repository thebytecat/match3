﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Match3
{
    public class LevelSelectItem : MonoBehaviour
    {
        [HideInInspector]
        public LevelInfo Level;
        public Text LevelNumber;
        public Button SelectButton;
        
        public void SelectLevel()
        {
            Debug.Log($"Select level {LevelNumber.text}");
            DataManager.Instance.CurrentLevel = Level;
            SceneManager.LoadScene("game");
        }

        public void Setup(LevelInfo level, int levelNumber)
        {
            Level = level;
            LevelNumber.text = levelNumber.ToString();
            SelectButton.onClick.AddListener(SelectLevel);
        }        
    }
}