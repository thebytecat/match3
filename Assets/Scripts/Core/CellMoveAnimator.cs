﻿using UnityEngine;

namespace Match3.Core
{
    public class CellMoveAnimator
    {
        private readonly CellAnimator cellAnimator;
        private readonly Cell cell;
        private Vector3 targetPositon;
        private Vector2Int moveToPosition;

        public CellMoveAnimator(Cell cell, CellAnimator cellAnimator)
        {
            this.cell = cell;
            this.cellAnimator = cellAnimator;
        }

        public void MoveTo(int column, int row)
        {
            moveToPosition = new Vector2Int(column, row);
            targetPositon = cell.transform.localPosition;
            targetPositon.Set(
                targetPositon.x - cell.Board.CellSize * (cell.Column - column),
                targetPositon.y - cell.Board.CellSize * (cell.Row - row), targetPositon.z);
        }

        public void MoveTo(Vector2Int position)
        {
            MoveTo(position.x, position.y);
        }

        public bool Move()
        {
            float step = cellAnimator.CellMovingSpeed * Time.deltaTime;
            cell.transform.localPosition = Vector3.MoveTowards(cell.transform.localPosition, targetPositon, step);
            if (Vector3.Distance(cell.transform.localPosition, targetPositon) < 0.1f) {
                cell.SetPosition(moveToPosition);
                return true;
            }
            return false;
        }
    }
}