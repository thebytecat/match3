﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Match3.Extension;

namespace Match3.Core
{
    public enum BoardState { Playing, Falling, CellMoving, Swapping }


    public class Board : MonoBehaviour
    {
        public float BoardScale { get; private set; }

        private HintFinder hintFinder;
        private InputHandler inputHandler;
        private CellAnimator cellAnimator;

        private Tuple<Cell, Cell> swappedCells;

        public GameObject CellPrefab;
        public GameObjectPool CellContainer;
        [Header("Board settings")]
        public Vector2Int Size = new Vector2Int(6, 6);
        public float CellSize = 2.56f;
        public Sprite[] Tiles;
        public RectTransform BoardRect;

        public event Action OnTurnEnd;
        public event Action<MatchResult> OnMatch;

        public bool IsPaused { get; set; }
        public BoardState State { get; set; }
        public Cell[,] Cells { get; private set; }
        public Bounds BoardBounds { get; set; }
        public int TileCount { get; set; }
        
        private void ScaleBoard()
        {
            var corners = new Vector3[4];
            BoardRect.GetWorldCorners(corners);
            var size = new Vector3(corners[2].x - corners[1].x, corners[1].y - corners[0].y);
            var center = new Vector3(corners[0].x + (size.x / 2), corners[0].y + (size.y / 2));
            BoardBounds = new Bounds(center, size);

            BoardScale = Mathf.Min(BoardBounds.size.x / (Size.x * CellSize), BoardBounds.size.y / (Size.y * CellSize));
            float realHalfCellSize = (CellSize * BoardScale / 2);
            CellContainer.transform.position = new Vector3(
                (BoardBounds.center.x - (Size.x * realHalfCellSize)) + realHalfCellSize,
                (BoardBounds.center.y - (Size.y * realHalfCellSize)) + realHalfCellSize
                );

            CellContainer.transform.localScale = new Vector3(BoardScale, BoardScale, 1);
        }

        private Cell GenerateCell(int column, int row)
        {
            var newCell = new Cell();
            var newGameObj = CellContainer.GetObject();

            newCell.Board = this;
            newCell.transform = newGameObj.GetComponent<Transform>();
            newCell.spriteRenderer = newGameObj.GetComponent<SpriteRenderer>();
            newCell.animator = newGameObj.GetComponent<Animator>();
            newCell.CellAnimator = new CellMoveAnimator(newCell, cellAnimator);
            newCell.SetPosition(column, row);
            newCell.Element = (CellElement)UnityEngine.Random.Range(0, TileCount);
            return newCell;
        }

        private void FilldBoard()
        {
            for (var x = 0; x < Size.x; x++) {
                int emptyCellInColumn = 0;
                for (var y = 0; y < Size.y; y++) {
                    if (Cells[x, y] == null) {
                        emptyCellInColumn++;
                    } else if (emptyCellInColumn > 0) {
                        cellAnimator.MoveDown(Cells[x, y], emptyCellInColumn);
                    }
                }

                if (emptyCellInColumn > 0) {
                    for (var i = 0; i < emptyCellInColumn; i++) {
                        var cell = GenerateCell(x, Size.y + i);
                        cellAnimator.MoveDown(cell, emptyCellInColumn);
                    }
                }
            }
        }

        private CellElement GetRandomElementExclude(IEnumerable<CellElement> exclude)
        {
            var excludeCollection = Enumerable.Range(0, TileCount).Where(x => !exclude.Contains((CellElement)x)).ToArray();
            int idx = UnityEngine.Random.Range(0, excludeCollection.Length);
            return (CellElement)excludeCollection[idx];
        }

        private void InitBoard()
        {
            var takenElements = new List<CellElement>(2);
            Cells = new Cell[Size.x, Size.y];
            for (var x = 0; x < Size.x; x++) {
                for (var y = 0; y < Size.y; y++) {
                    var cell = GenerateCell(x, y);
                    takenElements.Clear();
                    if (y > 1 && Cells[x, y - 1].Element == Cells[x, y - 2].Element)
                        takenElements.Add(Cells[x, y - 1].Element);
                    if (x > 1 && Cells[x - 1, y].Element == Cells[x - 2, y].Element)
                        takenElements.Add(Cells[x - 1, y].Element);
                    if (takenElements.Count > 0 && takenElements.Contains(cell.Element)) {
                        cell.Element = GetRandomElementExclude(takenElements);
                    }
                    Cells[x, y] = cell;
                }
            }
        }

        private void Start()
        {
            ScaleBoard();
            cellAnimator = new CellAnimator();
            InitBoard();
            hintFinder = new HintFinder(this);
            inputHandler = new InputHandler(this);
            State = BoardState.Playing;
        }

        public void SwapCells(Cell a, Cell b)
        {
            var tempPos = a.Position;
            cellAnimator.Move(a, b.Position);
            cellAnimator.Move(b, tempPos);
            State = BoardState.Swapping;

            swappedCells = new Tuple<Cell, Cell>(a, b);
        }

        public IEnumerable<IReadOnlyList<Cell>> Columns()
        {
            for (var x = 0; x < Size.x; x++) {
                var column = new Cell[Size.y];
                for (var y = 0; y < Size.y; y++) {
                    column[y] = Cells[x, y];
                }

                yield return column;
            }
        }

        public IEnumerable<IReadOnlyList<Cell>> Rows()
        {
            for (var y = 0; y < Size.y; y++) {
                var row = new Cell[Size.x];
                for (var x = 0; x < Size.x; x++) {
                    row[x] = Cells[x, y];
                }

                yield return row;
            }
        }

        private void FindAllMatchInLine(IReadOnlyList<Cell> line, MatchResult result)
        {
            var tempCells = new List<Cell> { line[0] };
            for (var i = 1; i < line.Count; i++) {
                var currentCell = line[i];

                if (currentCell.Element == tempCells[0].Element) {
                    tempCells.Add(currentCell);
                } else {
                    if (tempCells.Count >= 3) {
                        result.AddMatch(tempCells);
                    }
                    tempCells.Clear();
                    tempCells.Add(currentCell);
                }
            }

            if (tempCells.Count >= 3) {
                result.AddMatch(tempCells);
            }
        }

        private bool MatchAndClean()
        {
            var result = new MatchResult();

            foreach (var column in Columns()) {
                FindAllMatchInLine(column, result);
            }

            foreach (var row in Rows()) {
                FindAllMatchInLine(row, result);
            }

            if (result.Matches.Count > 0) {
                OnMatch?.Invoke(result);
            }

            foreach (var match in result.Matches) {
                foreach (var cell in match.Cells) {
                    ClearCell(cell);
                }
            }

            return result.Matches.Count > 0;
        }

        private void ClearCell(Cell cell)
        {
            if (cell != null) {
                Cells[cell.Column, cell.Row] = null;
                CellContainer.ReturnObject(cell.transform.gameObject);
            }
        }

        void Update()
        {
            if (IsPaused) {
                return;
            }

            if (State == BoardState.Playing) {
                hintFinder.Update();
                inputHandler.Update();
            }

            if (State == BoardState.Falling) {
                cellAnimator.Update();
                if (!cellAnimator.IsActive) {
                    if (MatchAndClean()) {
                        FilldBoard();
                    } else {
                        State = BoardState.Playing;
                        OnTurnEnd?.Invoke();
                    }
                }

            }

            if (State == BoardState.CellMoving || State == BoardState.Swapping) {
                cellAnimator.Update();
                if (!cellAnimator.IsActive) {
                    if (MatchAndClean()) {
                        FilldBoard();
                        State = BoardState.Falling;
                    } else if (State == BoardState.Swapping) {
                        SwapCells(swappedCells.Item1, swappedCells.Item2);
                        State = BoardState.CellMoving;
                    } else {
                        State = BoardState.Playing;
                    }
                }
            }
        }

        public void ShuffleBoard()
        {
            Debug.Log("Shuffle");
            Cells.Shuffle();

            for (int x = 0; x < Size.x; x++) {
                for (int y = 0; y < Size.y; y++) {
                    if (Cells[x, y] != null)
                        cellAnimator.Move(Cells[x, y], x, y);
                }
            }

            State = BoardState.CellMoving;
        }

    }
}
