﻿using System;
using System.Collections.Generic;
using System.Linq;
using Match3.Extension;
using UnityEngine;

namespace Match3.Core
{

    public class PosibleTurn
    {
        public Cell From, To;

        public List<Tuple<CellElement, int>> MatchResult { get; }

        public PosibleTurn(Cell from, Cell to)
        {
            From = from;
            To = to;
            MatchResult = new List<Tuple<CellElement, int>>();
        }
    }


    public class HintFinder
    {
        private readonly Board board;
        private Vector2Int size;
        private readonly Cell[,] testBoard;
        private PosibleTurn posibleTurn;
        private float timeToHint;

        public float HintDelay { get; set; }

        public HintFinder(Board board)
        {
            this.board = board;
            size = new Vector2Int(board.Cells.GetLength(0), board.Cells.GetLength(1));
            testBoard = new Cell[size.x, size.y];

            HintDelay = 3f;
            board.OnTurnEnd += UpdateHint;
        }

        /// <summary>
        /// Return length of match starting from position [column, row] in a given direction.
        /// Starting position is not counted.
        /// </summary>
        private int CheckDirection(int column, int row, Vector2Int direction)
        {
            int matchLength = 0;
            var targetElement = testBoard[column, row].Element;
            int x = column + direction.x;
            int y = row + direction.y;
            while (x >= 0 && y >= 0 && x < size.x && y < size.y && testBoard[x, y].Element == targetElement) {
                matchLength++;
                x += direction.x;
                y += direction.y;
            }
            return matchLength;
        }

        /// <summary>
        /// Return number of matched cells started from position [column, row].
        /// Return 0 if match don't found.
        /// </summary>
        /// <param name="column"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        private int CheckAllDirection(int column, int row)
        {
            int count = 0;
            //Check horizontal direction
            var hCount = CheckDirection(column, row, Vector2Int.left) + CheckDirection(column, row, Vector2Int.right) + 1;
            if (hCount >= 3)
                count += hCount;
            //Check vertical direction
            var vCount = CheckDirection(column, row, Vector2Int.up) + CheckDirection(column, row, Vector2Int.down) + 1;
            if (vCount >= 3)
                count += vCount;

            return count;
        }

        private IEnumerable<PosibleTurn> PosibleTurns()
        {
            PosibleTurn turn = null;
            board.Cells.CopyTo(testBoard);
            for (int x = 0; x < size.x; x++) {
                for (int y = 0; y < size.y; y++) {

                    //Use chess order to skip double checks
                    if ((x + y) % 2 == 0) {
                        Cell tempCell = testBoard[x, y];
                        int newX = x;
                        int newY = y;
                        //Check posible turn in the following order : Left, Down, Right, Up
                        for (int i = 0; i < 4; i++) {
                            newX = x + (i % 2 == 0 ? i - 1 : 0);
                            newY = y + (i % 2 == 1 ? i - 2 : 0);
                            //Skip out of board position
                            if (newX < 0 || newX >= size.x || newY < 0 || newY >= size.y)
                                continue;
                            //Skip cells with the same element 
                            if (testBoard[x, y].Element == testBoard[newX, newY].Element)
                                continue;
                            //Swap cells
                            testBoard[x, y] = testBoard[newX, newY];
                            testBoard[newX, newY] = tempCell;

                            int matchedCells = 0;
                            //Check matches in [x, y] position
                            matchedCells = CheckAllDirection(x, y);
                            if (matchedCells != 0) {
                                turn = new PosibleTurn(testBoard[x, y], testBoard[newX, newY]);
                                turn.MatchResult.Add(new Tuple<CellElement, int>(testBoard[x, y].Element, matchedCells));
                            }
                            //Check matches in [newX, newY] position
                            matchedCells = CheckAllDirection(newX, newY);
                            if (matchedCells != 0) {
                                if (turn == null) {
                                    turn = new PosibleTurn(testBoard[x, y], testBoard[newX, newY]);
                                }
                                turn.MatchResult.Add(new Tuple<CellElement, int>(testBoard[newX, newY].Element, matchedCells));
                            }

                            //Return result
                            if (turn != null) {
                                yield return turn;
                                turn = null;
                            }

                            //Swap cells back
                            testBoard[newX, newY] = testBoard[x, y];
                            testBoard[x, y] = tempCell;
                        }

                    }
                }
            }
        }

        private void UpdateHint()
        {
            posibleTurn = PosibleTurns().FirstOrDefault();
            if (posibleTurn == null) {
                board.ShuffleBoard();
            } else {
                timeToHint = HintDelay;
            }
        }

        public void Update()
        {
            if (posibleTurn != null) {
                timeToHint -= Time.deltaTime;
                if (timeToHint <= 0) {
                    posibleTurn.From.Mark();
                    posibleTurn.To.Mark();
                    timeToHint = HintDelay;
                }
            } else {
                UpdateHint();
            }
        }
    }
}