﻿using UnityEngine;

namespace Match3.Core
{
    public enum CellType { Empty, Element }
    public enum CellElement { None = -1, Milk, Apple, Lemon, Bread, Broccoli, Coconut, Crystal }

    public struct CellContent
    {
        public CellType Type;
        public CellElement Element;
    }

    public class Cell
    {
        // start from left-bottom
        private Vector2Int position;
        private CellContent content;
        private bool selected;
        // need for select animation
        private float selectScale = 1.1f;
        private Vector3 normalScale;

        public Transform transform;
        public SpriteRenderer spriteRenderer;
        public Animator animator;
        public Board Board;
        public CellMoveAnimator CellAnimator;

        public CellType Type
        {
            get
            {
                return content.Type;
            }

            set
            {
                if (value == CellType.Empty) {
                    content.Element = CellElement.None;
                }
                content.Type = value;
            }
        }
        public CellElement Element
        {
            get
            {
                return content.Element;
            }

            set
            {
                content.Type = value == CellElement.None ? CellType.Empty : CellType.Element;
                content.Element = value;

                if (content.Type != CellType.Empty) {
                    spriteRenderer.sprite = Board.Tiles[(int)content.Element];
                }
            }
        }
        public int Column
        {
            get { return position.x; }
            private set { position.x = value; }
        }
        public int Row
        {
            get { return position.y; }
            private set { position.y = value; }
        }
        public Vector2Int Position => position;

        public bool Selected
        {
            get
            {
                return selected;
            }

            set
            {
                selected = value;
                if (selected) {
                    normalScale = transform.localScale;
                    //TODO: Remove magic number
                    transform.localScale *= selectScale;
                } else {
                    transform.localScale = normalScale;
                }
            }
        }

        public Cell()
        {
            Type = CellType.Empty;
        }

        public bool IsNeighbor(Cell other)
        {
            return (Mathf.Abs(Column - other.Column) + Mathf.Abs(Row - other.Row)) == 1;
        }

        public void Mark()
        {
            animator.SetTrigger("marked");
        }

        public void SetPosition(int column, int row)
        {
            Column = column;
            Row = row;
            transform.localPosition = new Vector3(
                (Column * Board.CellSize),
                (Row * Board.CellSize),
                transform.position.z
                );
            if (Column < Board.Size.x
                && Column >= 0
                && Row < Board.Size.y
                && Row >= 0) {
                Board.Cells[Column, Row] = this;
            }
        }

        public void SetPosition(Vector2Int newPosition)
        {
            SetPosition(newPosition.x, newPosition.y);
        }
    }
}
