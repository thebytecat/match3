﻿using System.Collections.Generic;
using UnityEngine;

namespace Match3.Core
{
    public class CellAnimator
    {
        public float CellMovingSpeed = 10f;
        public bool IsActive { get; private set; }
        public List<CellMoveAnimator> MovingCells { get; set; }

        public CellAnimator()
        {
            MovingCells = new List<CellMoveAnimator>();
        }

        public void Move(Cell cell, int column, int row)
        {
            cell.CellAnimator.MoveTo(column, row);
            MovingCells.Add(cell.CellAnimator);
            IsActive = true;
        }

        public void Move(Cell cell, Vector2Int position)
        {
            Move(cell, position.x, position.y);
        }

        public void MoveDown(Cell cell, int deltaY)
        {
            Move(cell, cell.Column, cell.Row - deltaY);
        }

        public void Update()
        {
            if (MovingCells.Count > 0) {
                for (int i = 0; i < MovingCells.Count; i++) {
                    var cell = MovingCells[i];
                    if (cell.Move()) {
                        MovingCells.RemoveAt(i);
                    }
                }

                if (MovingCells.Count <= 0) {
                    IsActive = false;
                }
            }
        }
    }
}