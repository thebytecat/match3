﻿using UnityEngine;

namespace Match3.Core
{
    public class InputHandler
    {
        private readonly Board board;
        private Cell selectedCell;
        private float cellSize;
        private Vector2 boardStartPosition;

        public Cell SelectedCell
        {
            get
            {
                return selectedCell;
            }

            set
            {
                if (selectedCell != null) {
                    selectedCell.Selected = false;
                }
                if (value != null) {
                    value.Selected = true;
                }
                selectedCell = value;
            }
        }

        public InputHandler(Board board)
        {
            this.board = board;
            cellSize = board.CellSize * board.BoardScale;
            boardStartPosition = new Vector2(
                board.CellContainer.transform.position.x - (cellSize / 2),
                board.CellContainer.transform.position.y - (cellSize / 2)
                );
        }

        private Vector2Int WorldToBoardPosition(Vector2 position)
        {
            var boardPosition = (position - boardStartPosition) / cellSize;
            return Vector2Int.FloorToInt(boardPosition);
        }

        private Cell GetCell()
        {
            var mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePosition.Set(mousePosition.x, mousePosition.y, 0);
            if (!board.BoardBounds.Contains(mousePosition))
                return null;

            var boardPosition = WorldToBoardPosition(mousePosition);
            if (boardPosition.x >= board.Size.x || boardPosition.y >= board.Size.y || boardPosition.x < 0 || boardPosition.y < 0)
                return null;

            return board.Cells[boardPosition.x, boardPosition.y];
        }

        public void Update()
        {
            if (Input.GetMouseButtonDown(0)) {
                var targetCell = GetCell();
                if (targetCell == null)
                    return;
                if (SelectedCell == null) {
                    SelectedCell = targetCell;
                } else if (SelectedCell == targetCell) {
                    SelectedCell = null;
                } else {
                    if (selectedCell.IsNeighbor(targetCell)) {
                        board.SwapCells(SelectedCell, targetCell);
                        SelectedCell = null;
                    } else {
                        SelectedCell = targetCell;
                    }
                }

                return;
            }

            if (Input.GetMouseButton(0)) {
                if (SelectedCell == null)
                    return;

                var targetCell = GetCell();
                if (targetCell == null)
                    return;
                if (SelectedCell != targetCell) {
                    if (selectedCell.IsNeighbor(targetCell)) {
                        board.SwapCells(SelectedCell, targetCell);
                        SelectedCell = null;
                    }
                }
            }

        }
    }
}