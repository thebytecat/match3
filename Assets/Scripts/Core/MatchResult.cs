﻿using System.Collections.Generic;
using System.Linq;

namespace Match3.Core
{
    public class MatchResult
    {
        public List<Match> Matches { get; }

        public int TotalCount
        {
            get
            {
                return Matches.Count > 0 ? Matches.Sum(m => m.Cells.Count) : 0;
            }
        }

        public MatchResult()
        {
            Matches = new List<Match>();
        }

        public void AddMatch(List<Cell> cells)
        {
            var element = cells.First().Element;
            foreach (var match in Matches) {
                if (element == match.Element) {
                    var intersect = cells.Except(match.Cells).ToArray();
                    if (intersect.Length < cells.Count ) {
                        match.Cells.AddRange(intersect);
                        return;
                    }
                }
            }
            Matches.Add(new Match(cells));
        }
    }

    public class Match
    {
        public List<Cell> Cells { get; }
        public CellElement Element { get; }

        public Match(List<Cell> cells)
        {
            if (cells != null) {
                Cells = cells.ToList();
                Element = Cells.First().Element;
            } else {
                Cells = new List<Cell>();
                Element = CellElement.None;
            }
        }

        public Match()
        {
            Cells = new List<Cell>();
            Element = CellElement.None;
        }
    }
}