﻿using System;
using Match3;
using Match3.Core;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    [RequireComponent(typeof(AudioSource))]
    public class SoundController : MonoBehaviour
    {
        private AudioSource source;
        private Board board;
        
        public AudioClip BackgroundMusic;
        public AudioClip MatchSound;

        private void Awake()
        {
            if (FindObjectsOfType<SoundController>().Length > 1) {
                Destroy(gameObject);
                return;
            }

            source = GetComponent<AudioSource>();
            if (SceneManager.GetActiveScene().name == "game") {
                board = FindObjectOfType<Board>();
                if (board != null) {
                    board.OnMatch += PlayMatchSound;
                }
            }
            SceneManager.sceneLoaded += SceneLoaded;
            DontDestroyOnLoad(gameObject);
        }

        private void SceneLoaded(Scene scene, LoadSceneMode mode)
        {
            if (board != null) {
                board.OnMatch -= PlayMatchSound;
                board = null;
            }
            
            if (scene.name == "game") {
                board = FindObjectOfType<Board>();
                if (board != null) {
                    board.OnMatch += PlayMatchSound;
                }
            }
        }

        private void Start()
        {
            source.mute = DataManager.Instance.AudioSettings.MuteSound;
            source.clip = BackgroundMusic;
            source.Play();
        }

        private void PlayMatchSound(MatchResult matchResult)
        {
                source.PlayOneShot(MatchSound);
        }

        public void SetMute()
        {
            DataManager.Instance.AudioSettings.MuteSound = !DataManager.Instance.AudioSettings.MuteSound;
            source.mute = DataManager.Instance.AudioSettings.MuteSound;
        }
    }
}