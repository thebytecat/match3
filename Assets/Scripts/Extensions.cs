﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3.Extension
{
    public static class Extensions
    {
        // Shuffle 2D array by Fisher-Yates algorithm
        public static void Shuffle<T>(this T[,] array)
        {
            int lengthRow = array.GetLength(1);

            for (int i = array.Length - 1; i > 0; i--) {
                int i0 = i / lengthRow;
                int i1 = i % lengthRow;

                int j = UnityEngine.Random.Range(0, i + 1);
                int j0 = j / lengthRow;
                int j1 = j % lengthRow;

                T temp = array[i0, i1];
                array[i0, i1] = array[j0, j1];
                array[j0, j1] = temp;
            }
        }

        public static void CopyTo<T>(this T[,] source, T[,] distination)
        {
            int maxX = source.GetLength(0);
            int maxY = source.GetLength(1);

            for (int x = 0; x < maxX; x++)
            {
                for (int y = 0; y < maxY; y++)
                {
                    distination[x, y] = source[x, y];
                }
            }
        }
    }
}
