﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Match3;

public class DataManager : MonoBehaviour
{
    private static DataManager instance;

    public static DataManager Instance {
        get {
            if (instance == null) {
                var obj = new GameObject("DataManager");
                instance = obj.AddComponent<DataManager>();
            }

            return instance;
        }
    }

    public LevelInfo[] Levels { get; private set; }
    public LevelInfo CurrentLevel { get; set; }

    public GameAudioSettings AudioSettings;

    private void Awake()
    {
        //Singletone initialization
        if (instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        LoadData();
    }

    private void LoadData()
    {
        Levels = Resources.LoadAll<LevelInfo>("Levels");
        CurrentLevel = Levels.First();
        AudioSettings = new GameAudioSettings();
        if (PlayerPrefs.HasKey("sound_mute")) {
            AudioSettings.MuteSound = PlayerPrefs.GetInt("sound_mute") != 0;
        }
        else {
            AudioSettings.MuteSound = false;
            PlayerPrefs.SetInt("sound_mute", 0) ;
        }
    }
}

public class GameAudioSettings
{
    private bool muteSound;

    public bool MuteSound
    {
        get
        {
            return muteSound;
        }

        set
        {
            muteSound = value;
            PlayerPrefs.SetInt("sound_mute", muteSound ? 1 : 0);
        }
    }
}
