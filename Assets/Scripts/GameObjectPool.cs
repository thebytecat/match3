﻿using UnityEngine;
using System.Collections.Generic;

public class GameObjectPool : MonoBehaviour
{
    private readonly Stack<GameObject> inactiveInstances = new Stack<GameObject>();

    public GameObject prefab;

    public GameObject GetObject()
    {
        GameObject spawnedGameObject;
        
        spawnedGameObject = inactiveInstances.Count > 0 ? inactiveInstances.Pop() : Instantiate(prefab, transform);
        
        spawnedGameObject.transform.rotation = Quaternion.identity;
        spawnedGameObject.SetActive(true);
        
        return spawnedGameObject;
    }
    
    public void ReturnObject(GameObject toReturn)
    {        
        if (toReturn != null) {
            //toReturn.transform.SetParent(transform);
            toReturn.SetActive(false);
            
            inactiveInstances.Push(toReturn);
        }
    }
}