﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.UI;

public class SoundSetter : MonoBehaviour
{
    private bool state;
    private SoundController soundController;

    public Image Image;
    public Sprite StateOnSprite;
    public Sprite StateOffSprite;

    private void Start ()
    {
        soundController = FindObjectOfType<SoundController>();
        state = !DataManager.Instance.AudioSettings.MuteSound;
        Image.sprite = state ? StateOnSprite : StateOffSprite;
    }

    public void SetState()
    {
        soundController.SetMute();
        state = !state;
        Image.sprite = state ? StateOnSprite : StateOffSprite;
    }
}
