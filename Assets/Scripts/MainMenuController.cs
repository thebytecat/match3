﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Match3
{
    class MainMenuController : MonoBehaviour
    {
        public LevelSelectItem LevelSelectPrefab;
        public Transform LevelsContainer;

        private void Start()
        {
            for (int i = 0; i < DataManager.Instance.Levels.Length; i++) {
                var level = DataManager.Instance.Levels[i];
                var levelSelectItem = Instantiate(LevelSelectPrefab, LevelsContainer);
                levelSelectItem.Setup(level, i + 1);
            }
        }
    }
}
