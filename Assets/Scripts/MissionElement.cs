﻿using System.Collections;
using System.Collections.Generic;
using Match3.Core;
using UnityEngine;
using UnityEngine.UI;

namespace Match3
{

    public class MissionElement : MonoBehaviour
    {
        private int elementLeft;

        public Text ElementLeftLable;
        public Image ElementTile;
        public Image CompleteTaskImage;

        public int ElementLeft
        {
            get
            {
                return elementLeft;
            }

            set
            {
                elementLeft = Mathf.Max(0, value);
                ElementLeftLable.text = elementLeft.ToString();
                if (elementLeft <= 0) {
                    CompleteTaskImage.gameObject.SetActive(true);
                }
            }
        }

        public CellElement Element { get; set; }
    }
}
