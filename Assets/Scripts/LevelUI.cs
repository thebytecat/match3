﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUI : MonoBehaviour {

    public GameObject GameOverPanel;
    public GameObject WinPanel;
    public GameObject PausePanel;

    public void ShowWinPanel()
    {
        WinPanel.SetActive(true);
    }

    public void ShowGameOverPanel()
    {
        GameOverPanel.SetActive(true);
    }

    public void ShowPausePanel()
    {
        PausePanel.SetActive(true);
    }

    public void HidePausePanel()
    {
        PausePanel.SetActive(false);
    }
    
}
